package utils

import groovy.io.FileType

class JobDslUtils{
    static String[] get_folders(path){
        def list = []

        def dir = new File(path)
        dir.eachFile (FileType.DIRECTORIES) { file ->
            list << file.getName()
        }
        return list
    }
}
