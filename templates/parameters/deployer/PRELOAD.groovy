import hudson.model.User
import jenkins.security.ApiTokenProperty;
import java.security.MessageDigest;
import java.net.URL;
import groovy.json.JsonSlurperClassic;

def jsonParse(json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
    }

currentToken = User.current().getProperty(ApiTokenProperty.class).getApiTokenInsecure()
MessageDigest digest = MessageDigest.getInstance("SHA-256") ;
digest.update(currentToken.bytes);
hashKey = new BigInteger(1, digest.digest()).toString(16).padLeft(32, '0');


def inf_param =''


return """
<link href='/userContent/bootstrap_allert.css' rel='stylesheet' />
<script src='/userContent/bootstrap_allert.js' type='text/javascript'></script>
${inf_param}

<!-- Обработка событий -->
<script type='text/javascript'>
function triggerChangeEvent(id_element){
Q('#'+id_element).trigger( "change" );
}
function seeSecret(id,seeid){
   Q('#'+id).attr('type', 'text');
   Q('#'+seeid).attr('value', 'Скрыть');
}

function hideSecret(id,seeid){
   Q('#'+id).attr('type', 'password');
   Q('#'+seeid).attr('value', 'Показать');
}

// https://stackoverflow.com/questions/33529103/simply-xor-encrypt-in-javascript-and-decrypt-in-java
function encryptStringWithXORtoHex(input,key) {
    var c = '';
    while (key.length < input.length) {
         key += key;
    }
    for(var i=0; i<input.length; i++) {
        var value1 = input[i].charCodeAt(0);
        var value2 = key[i].charCodeAt(0);

        var xorValue = value1 ^ value2;

        var xorValueAsHexString = xorValue.toString("16");

        if (xorValueAsHexString.length < 2) {
            xorValueAsHexString = "0" + xorValueAsHexString;
        }

        c += xorValueAsHexString;
    }
    return c;
}

function encryptVars(value){
  var cryptPub_key = "${hashKey}"
  value = encryptStringWithXORtoHex(value,cryptPub_key)
  return value
}

//Получение результирующего набора данных
function getContent(){
   var retObject = {}
   var table = document.getElementById('k_v_table');
   var currentTableLength = table.rows.length
   for (var j = 1; j < currentTableLength; j++) {
     //console.log(Q('#key_extra_vars_'+j).val())
     //console.log(Q('#value_extra_vars_'+j).val())
     retObject[Q('#key_extra_vars_'+j).val()]=encryptVars(Q('#value_extra_vars_'+j).val())
   }
   Q('#VAULT_PARAMS').attr('value', JSON.stringify(retObject));
};


//Функция по добавлению элементов в таблицу и привязывание к ним событий на внесение данных
function addElement() {
	var table = document.getElementById('k_v_table');
    var currentTableLength = table.rows.length
    var elemetnsId;
    if (currentTableLength == 0){
     elemetnsId = currentTableLength+1
    }else{
     elemetnsId = currentTableLength
    }
    var keyId = 'key_extra_vars_'+ elemetnsId
    var valueId = 'value_extra_vars_'+ elemetnsId
    var seeId = 'see_btn_'+ elemetnsId
    if (currentTableLength == 0){
      var head_row = table.insertRow()
      head_cell_1 = head_row.insertCell(0);
      head_cell_2 = head_row.insertCell(1);
      head_cell_3 = head_row.insertCell(2);
      head_cell_1.innerHTML ='Ключ';
      head_cell_2.innerHTML ='Значение';
      head_cell_3.innerHTML ='';
    }
    var newRow = table.insertRow();
	var cell1 = newRow.insertCell(0);
    var cell2 = newRow.insertCell(1);
    var cell3 = newRow.insertCell(2);
    var feld = document.createElement("input");
    feld.id = keyId
    feld.setAttribute('style', 'width:100%');
    feld.addEventListener('input',() => { getContent();}, false);
    feld.required = true
    feld.placeholder="Имя переменной"

    var masked_feld = document.createElement("input");
    masked_feld.type='password'
    masked_feld.id = valueId
    masked_feld.setAttribute('style', 'width:100%');
    masked_feld.required = true
    masked_feld.placeholder="Значение"
    masked_feld.addEventListener('input',() => { getContent();}, false);

    var see_btn = document.createElement("input");
    see_btn.setAttribute('type','button');
    see_btn.value='Показать'
    see_btn.id = seeId
    see_btn.setAttribute('style', 'width:100%');
    see_btn.addEventListener('mousedown',() => { seeSecret(valueId,seeId);}, false);
    see_btn.addEventListener('mouseup',() => { hideSecret(valueId,seeId); }, false);

    cell1.appendChild(feld);
    cell2.appendChild(masked_feld);
    cell3.appendChild(see_btn);
    Q('#del_element').attr('disabled', false);
}

//Функция очистки строки и результирующего набора данных
function remElement() {
    var table = document.getElementById('k_v_table');
    var currentTableLength = table.rows.length-1
    table.deleteRow(currentTableLength)
    if (currentTableLength == 1){
        table.deleteRow(currentTableLength-1)
        Q('#del_element').attr('disabled', true);
    }
    getContent()
}
</script>
"""
