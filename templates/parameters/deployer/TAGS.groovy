import org.eclipse.jgit.api.Git;
import org.jenkinsci.plugins.pipeline.utility.steps.shaded.org.yaml.snakeyaml.Yaml
import groovy.json.JsonOutput

def jsonDump(def data) {
        //but if writeJSON not supported by your version:
        //convert maps/arrays to json formatted string
        def json = JsonOutput.toJson(data)
        //if you need pretty print (multiline) json
        json = JsonOutput.prettyPrint(json)
        return json
    }


def jsonParse(def json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
    }
def parseYaml(filepath){
    Yaml parser = new Yaml()
    def res = parser.load((filepath as File).text)
    return res
  }

class PlaybookParser implements Serializable{
  
  private final String workdir
  private final String rootPlaybookfilename
  private playbookStruct = [:]
  
  PlaybookParser(workdir, rootPlaybookfilename) {
        this.workdir = workdir
    	this.rootPlaybookfilename = rootPlaybookfilename
   }
  
  def parseYaml(filepath){
    Yaml parser = new Yaml()
    def res = parser.load((filepath as File).text)
    return res
  }
  
  def generatePlaybookStruct(filename){
    return [[("${filename}".toString()): parseYaml("${this.workdir}/${filename}")]]
  }
  
  def appendIncludes(playbooksList){
     def result = []
     def tags_include = []
    playbooksList.each{ playbook ->
      playbook.each{ key, content ->
        content.each{ section ->
          if (section.containsKey("tags")){
            if (section["tags"] instanceof String){
            	tags_include += [section["tags"]]
            }else{
            	tags_include.add(section["tags"])
            }
          }
          
          if (section.containsKey("include")){
          	def buf_scr = this.generatePlaybookStruct(section['include'])
            if (buf_scr){
            def include = this.appendIncludes(buf_scr)
              include.each{ i_items ->
                if (i_items['tags'].size()>0){
                	i_items['tags'] += tags_include
                }else{
                	i_items['tags']=tags_include
                }
                result = result + include
              }
                
            }
                            
          }else if (section.containsKey("import_playbook")){
          def buf_scr = this.generatePlaybookStruct(section['import_playbook'])
            if (buf_scr){
            def  include = this.appendIncludes(buf_scr)
              include.each{ i_items ->
                if (i_items['tags'].size()>0){
                	i_items['tags'] += tags_include
                }else{
                	i_items['tags']=tags_include
                }
                result = result + include
              }
                
            }
          }else{
          result.add(section << ["tags": tags_include])
          }
        
        }
      }
    }
    
    return result
    
  }
  
  void init(){
    def playbooks = this.generatePlaybookStruct(this.rootPlaybookfilename)
    this.playbookStruct = this.appendIncludes(playbooks)
  }

def tags(){
      def all_tags = []
      this.playbookStruct.each{ play ->
          if (play.roles && play.roles.size()>0){
              play.roles.each{ role ->
                all_tags += role.tags
              }
          }
          
          if (play.tasks && play.tasks.size()>0){
              play.tasks.each{ task ->
                all_tags += task.tags
              }
          }
          
          all_tags += play.tags
          
      }
     return all_tags
  }

  
}


def jobsJson = jsonParse(JOBS);


def inventoryBufDir = jobsJson.INVENTORY_PROJECT.replaceAll(/(https|http):\/+.*?\//,'').replace('.git','')
def branch = BRANCH

File inventoryWorkDir = new File('/var/jenkins_home/caches/', "${inventoryBufDir}/${branch}");
def extra_vars = parseYaml("${inventoryWorkDir}/extra_vars.yml")
def play_repo_name = extra_vars.artifact.path
println(play_repo_name)
def play_branch = extra_vars.artifact.version
def playbook_path = jobsJson.PLAYBOOK

def BufDir = play_repo_name.replaceAll(/(https|http):\/+.*?\//,'').replace('.git','')
File workDir = new File('/var/jenkins_home/caches/', "${BufDir}/${play_branch}");
boolean exists = workDir.exists();

if (! exists){
  Git.cloneRepository().setURI(play_repo_name).setDirectory(workDir)
  .setBare(false)
  .setBranch( "refs/heads/${play_branch}" )
  .setNoCheckout(false)
  .call().close();
}

Git git = Git.open(workDir);
git.pull().call();
git.close();

PlaybookParser pp = new PlaybookParser(workDir, playbook_path)
pp.init()

println pp.tags()
return pp.tags()
