//def jobsPath = 'CD_PIPELINES' - проставляется при генерации джоба
import org.boon.Boon;

//Далее инициализируем ве необходимые параметры
def ignoreJobs = [ 'JOBS_STARTER' ]
def requiredParameters = [ 'INVENTORY_PROJECT', 'PLAYBOOK', 'GIT_HTTP_URL' ]
def optionalParameters = [ 'INVENTORY_PATH' ]
//def jobName = binding.jenkinsProject.name

//Валидируем INVENTORY_PATH
boolean validate_inventory_path(inventory){
  // inventory path should not contain templating pattrn {
    return !(inventory =~ /\{/)
}

// Получаем опциональные параметры
def getOptionalProps(item,optionalParameters){
    def result= [:]
    optionalParameters.each{ p ->
                            def paramValue = item.getProperty(hudson.model.ParametersDefinitionProperty)
                         ?.getParameterDefinitions()
                         ?.find{
                                 it.getName() == p  //Ищем INVENTORY_PATH
                         }?.getDefaultParameterValue()?.getValue()
      			if (paramValue != null) { result << [ "${p}": paramValue]}

                       }
	return result
}


//def jobsPath = (Thread.currentThread().toString() =~ /^.*?(\/.*\/)(.*)(\/build)/)[0][1].replaceAll('/job/','')

def allJobs = jenkins.model.Jenkins.getInstance().getAllItems().findAll { it instanceof hudson.model.Job &&  it.getFullName() =~ (jobsPath + '/PRODUCTS') }
//Получаем коллекцию нужных нам джобов
def suitableJobs = allJobs.findAll{! ignoreJobs.contains(it.name) }.collect {
                              [
                                name: it.name, // Имя джоба
                                fullName: it.getFullName() , // полное имя джоба
                                defaults:
                                               it.getProperty(hudson.model.ParametersDefinitionProperty)
                                               ?.getParameterDefinitions()
                                               ?.findAll {
                                                       requiredParameters.contains(it.getName())  //Ищем требуемые параметры
                                               }?.collectEntries {
                                                       [ it.getName(), it.getDefaultParameterValue().getValue() ] //Преобразуем список в Map
                                               } <<
                                                 getOptionalProps(it, optionalParameters) // добавляем опциональные параметры
                              ]
                     }.findAll {
                                it.defaults && it.defaults.size() >0 && validate_inventory_path(it.defaults.INVENTORY_PROJECT)  //Берем только те джобы, у которых найдены все требуемые параметры
                    }.collect{e -> [ 								// преобразуем данные в формат для select2
                                    text: e.name,
                                    id: Boon.toJson( e.defaults << [fullname: e.fullName, jobName: e.name] )
                    ]
                    }.sort{ it.text }

//Дампим в JSON
def first_value;
if (suitableJobs.size()>0) {
first_value = suitableJobs[0].id
}else{
  first_value=''
}
def json_data = Boon.toJson(suitableJobs)

//Возвращаем html на форму
html =
"""
<!-- import js and css -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<!-- select element -->
<select id='project_jobs' ></select>
<input type='hidden' id=JOBS1 name='value' value=${first_value}>

<script type='text/javascript'>

Q('#project_jobs').ready(function(){
    Q('#project_jobs').select2({width: '100%', data: ${json_data} });
});

Q(document).ready(function(){
    // on changed option
    Q('#project_jobs').on('change', function(e) {
        // Access to full data
        if ( Q(this).val() != null ){
        return_vale = Q(this).val()
         Q('#JOBS1').attr('value', return_vale );
        }else{
        return_vale = ''
         Q('#JOBS1').attr('value', return_vale );
        }
    });

    Q('.select2-multiple').select2();
});

</script>
"""
return html