import org.eclipse.jgit.api.Git;
import groovy.json.JsonOutput
import groovy.text.GStringTemplateEngine

def templateText(String text, def binding = [:]){
        //def engine = new groovy.text.SimpleTemplateEngine()
        String res = new groovy.text.GStringTemplateEngine().createTemplate(text).make(binding).toString()
        return res
    }


def jsonDump(def data) {
        //but if writeJSON not supported by your version:
        //convert maps/arrays to json formatted string
        def json = JsonOutput.toJson(data)
        //if you need pretty print (multiline) json
        json = JsonOutput.prettyPrint(json)
        return json
    }

def jsonParse(def json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
    }
def jobsJson = jsonParse(JOBS);

def repo_name = jobsJson.INVENTORY_PROJECT
def bufDir = repo_name.replaceAll(/(https|http):\/+.*?\//,'').replace('.git','')
def branch = BRANCH
def zone = ZONE


File workDir = new File('/var/jenkins_home/caches/', "${bufDir}/${branch}");
boolean exists = workDir.exists();
if (! exists){
  Git.cloneRepository().setURI(jobsJson.GIT_HTTP_URL+repo_name).setDirectory(workDir)
  .setBare(false)
  .setBranch( "refs/heads/${branch}" )
  .setNoCheckout(false)
  .call().close();
}

Git git = Git.open(workDir);
git.pull().call();
git.close();

def sout = new StringBuilder(), serr = new StringBuilder()
String command = "ansible-inventory -i ${workDir}/${zone} --list -vvv"
println command
def proc = command.execute()
proc.consumeProcessOutput(sout, serr)
proc.waitForOrKill(10000)

infoWarning = sout.substring(0, sout.indexOf("\n{"))
inventoryString =  sout.substring(sout.indexOf("\n{"))

inventory = jsonParse(inventoryString)
inventory.remove('_meta')
limits = [:]
inventory.each{ g, i ->
  if(g == 'all'){
    limits << i.children.collectEntries{it-> [  "${it}": "Group: ${it}"]}
  }else{
    if (g != 'ungrouped'){
      if(i.containsKey('children')){
    	limits << i.children.collectEntries{it-> ["${it}": "Group: ${it}" ]}
    }
    if (i.containsKey('hosts')){
    	limits << i.hosts.collectEntries{it-> [ "${it}": "Host(${g}): ${it}" ]}
    }
    }
  }
 }

def res =  limits.collectEntries{ [(it.key.toString()): it.value.toString()] }.sort { e1, e2 -> e1.value <=> e2.value }

println res
return res