// def INVENTORY_PROJECT

if (ZONE != "production"){
def gettags = ("git ls-remote -h ${INVENTORY_PROJECT}").execute()
def res = gettags.text.readLines().collect { 
  it.split()[1].replaceAll('refs/heads/', '').replaceAll('refs/tags/', '').replaceAll("\\^\\{\\}", '')
}
res.sort()
return res
}else{
    return ["master"]
}
