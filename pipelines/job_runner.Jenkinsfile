@Library('sharedlibraryexample@master') _
import jobrunner.Common
import groovy.json.JsonOutput
Common common = new Common(this);

//Проверка значений переменных
if( LIMITS == ''){
    common.abortPipeline('LIMITS can`t be empty!!!!!', true, true)
}

//Инициализация переменных
def jobName = currentBuild.rawBuild.project.displayName
// Инициализация форков
def forks = (params.FORKS ) ? FORKS : "5"

// Инициализация параметров запускаемого джоба
def parsedJsonData = common.jsonParse(JOBS);

// Набор параметров для окна подтверждения
def userInput = [:]

//Переменные для работы с бд и прочие секретные переменные
def secretExtraVars = (params.SECRET_EXTRA_VARS ) ? common.jsonParse(SECRET_EXTRA_VARS) : [:]



// Добавляем опции для основного плэйбука
/*
* Для возможности опционального отключения опций, используется параметр
* IGNORING_MODES, входящий в состав запускаемых джобов, там где это необходимо.
*
*/
def extras = []

/*
* Функция предназначена для проверки секретных переменных
* @param key, @param value - ключ и его значение для переменной
* @param common - иснтанс common-библиотеки
* @param secretKey - ключ для дешифровки значения переменных
* @return возвращает результирующий набор переменных, либо ошибку,
*    в случае не соответствия паттернам
*/
def searchInKeysValues(key,value,common,secretKey){
    c = common
    //Инициализируем необходимые паттерны для поиска ошибок в key, value
    res = [
            result: false,
            listSecretKeys: [
                    'password',
                    'pwd',
                    'syspass',
                    'pass',
                    'start_command',
                    'stop_command',
                    'restart_command'
            ],
            errorKeysPatterns: /([\^\&\?\!\@\|\\\/\>\<\$\#\%;`'":=,.{}])|(\s)/,
            errorValuesPatterns: /([`].*[`])|(['])/
    ]
    //Проверка ключа на наличие ошибок
    resKeyCollect = c.matchInVars(key,res.errorKeysPatterns)
    if (resKeyCollect.size() != 0){
        return [key: key, value: "ERROR: Find error in key: \'${key}\'. Key contains errors symbols. Please add correct key"]
    }
    //Проверка ключа на соответствие паттернам
    res.listSecretKeys.each{
        if (key.toLowerCase().contains(it) || key.toLowerCase() == it) {
            //echo('FIND')
            res.result = true
            return true;
        }
    }
    if (res.result == false){
        return [key: key, value: "ERROR: Not valid key: \'${key}\'. This key is not permitted to use in deploy extra_vars. Please add correct key"]
    }
    // Расшифровываем значение
    value = c.decryptStringWithXORFromHex(value,secretKey)
    //Проверка значения на соответствие паттернам
    resValueCollect = c.matchInVars(value,res.errorValuesPatterns)
    if (resValueCollect.size() != 0){
        return [key: key, value: "ERROR: Value from key: \'${key}\' contains a non-allowed character. Please add correct value"]
    }

    return [key: key, value: "'"+value+"'"]
}


/*
* Функция для дешифровки секретных параметров и поиска в них ошибок
* @param varsObj - map объект, содержащий секретные параметры
* @param secretKey - ключ для дешифровки значения переменных
* @param common - инстанс common-библиотеки
*/
def decryptVars(varsObj,secretKey,common){
    retVarsObj = [:]
    c = common
    //Собираем объект готовых переменных и их ошибок
    varsObj.each{ k,v ->
        res = searchInKeysValues(k,v,c,secretKey)
        retVarsObj[res.key]=res.value
    }
    def errorsVars = [:]
    //Производим проверку и вывод соответсвующих ошибок
    if (retVarsObj.size()>0){
        retVarsObj.each{ k,v ->
            if (v.contains('ERROR')){
                echo(v)
                errorsVars << ["${k}": v ]
            }
        }
        if (errorsVars.size()>0){
            c.abortPipeline('Find errors from SECRET_EXTRA_VARS parameter: ' + JsonOutput.prettyPrint(JsonOutput.toJson(errorsVars)), true, true)
        }
    }
    return retVarsObj
}

//Получаем секртный токен
def hashKey = common.get_current_token(currentBuild);
// Получаем экстраварсы из зашифрованных переменных
def decryptVars = (secretExtraVars == [:]) ? '' : JsonOutput.toJson(decryptVars(secretExtraVars,hashKey,common));

// Инициализируем набор переменных, которые будут переданы результирующему джобу
def VARS = [
        'USERNAME': USERNAME,
        'RUN_JOB_NAME': parsedJsonData.jobName,
        'RUN_JOB_FULLNAME': parsedJsonData.fullname,
        'PLAYBOOK': parsedJsonData.PLAYBOOK,
        'FORKS': forks,
        'VERBOSE': VERBOSE,
        'BRANCH': BRANCH,
        'LIMITS': LIMITS,
        'TAGS': TAGS,
        'ZONE': ZONE,
        'PASSWORD': PASSWORD,
        'AGENT_LABEL': AGENT_LABEL,
        'SECRET_EXTRA_VARS': decryptVars,
        'EXTRA_OPTIONS': extras.join(' ')
];

/*
* Функция для преобразования переменных джоба в параметры для его запуска
*/
def get_Deploy_Parameter(key,value){
    def return_map_collect = [];
    if (key in ['RUN_JOB_NAME','RUN_JOB_FULLNAME','USERNAME']){
        return []
    }else if (key in ['PASSWORD', 'SECRET_EXTRA_VARS'] ){
        return [$class: 'com.michelin.cio.hudson.plugins.passwordparam.PasswordParameterValue', name: key, value: value]
    }else{
        return [$class: 'WReadonlyStringParameterValue', name: key, description: '', value: value]
    }
}

/*
* Функция для генерации параметра ParameterSeparatorDefinition
* Меняет цвет в зависимости от значения @param value
* @param name - имя параметра
* @param label - видимое для пользователя описание
* @param value - значение параметра
*/
@NonCPS
def generateChangeColorSeparator(name,label,value){
    def color;
    switch (value) {
        case { it <=5}:
            color='green'
            break
        case { it >5 && it <10}:
            color='orange'
            break
        case { it >=10 }:
            color='red'
            break
    }
    String sectionHeaderStyle = """
        color: white;
        background: ${color};
        font-family: Roboto, sans-serif !important;
        padding: 5px;
        text-align: center;
    """
    //echo(sectionHeaderStyle)
    String separatorStyle = """
        border: 0;
        border-bottom: 1px dashed #ccc;
        background: #999;
    """
    def res = [
            $class: 'ParameterSeparatorDefinition',
            name: 'FOO_HEADER_'+name,
            sectionHeader: label,
            separatorStyle: separatorStyle,
            sectionHeaderStyle: sectionHeaderStyle
    ]

    return res
}

/*
* Функция для генерации параметра ParameterSeparatorDefinition
* с заданием определенного цвета
* @param name - имя параметра
* @param value - видимое для пользователя описание
* @param color - цвет лини сепаратора
*/
@NonCPS
def generateColorSeparator(name,value,color){
    String sectionHeaderStyle = """
        color: white;
        background: ${color};
        font-family: Roboto, sans-serif !important;
        padding: 5px;
        text-align: center;
    """
    //echo(sectionHeaderStyle)
    String separatorStyle = """
        border: 0;
        border-bottom: 1px dashed #ccc;
        background: #999;
    """
    def res = [
            $class: 'ParameterSeparatorDefinition',
            name: 'FOO_COLOR_HEADER_'+name,
            sectionHeader: value,
            separatorStyle: separatorStyle,
            sectionHeaderStyle: sectionHeaderStyle
    ]

    return res
}

/*
* Функция для выборки параметров по джобу, необходима для исключения не нужных ошибок.
* @param triggeredParametersDefinisions - массив имен параметров, передаваемых джобу
*/
@NonCPS
def excludeParameters(triggeredParametersDefinisions,jobName,common){
    def triggeredJobProps = common.get_all_props(common.get_instance_project(jobName))
    def result = []
    triggeredJobProps.each{ param ->
        def buff = triggeredParametersDefinisions.find{it.name == param}
        if (buff) {result << buff}
    }
    return result
}



//Получение текущего URL адреса билда
def currentBuildURL = currentBuild.absoluteUrl
currentBuild.displayName = "#${BUILD_NUMBER}_${parsedJsonData.jobName}_${USERNAME}"
currentBuild.description = "Build number: ${BUILD_NUMBER}, TriggeredJob: ${parsedJsonData.jobName}, BuildUser: ${USERNAME}"

/*
* Базовый пайплайн для запуска основных джобов и инициализации переменных
*/
try{
    stage("Init Parameters"){
        // Инициализация массива переменных, передаваемых в окно пдтверждения
        def input_parameters = [];

        // Вывод предупреждения о зацпуске полной установки
        if (TAGS == '' || TAGS == '-t all'){
            def tags_separator = generateColorSeparator('tags',"ВНИМАНИЕ!!! БУДЕТ ПРОИЗВЕДЕН ПОЛНЫЙ ДЕПЛОЙ ПРИЛОЖЕНИЯ", 'orange');
            input_parameters << tags_separator
        }


        // Вывод предупреждений об указании номера работ
        input_parameters << generateColorSeparator('ticket',"УКАЖИТЕ НОМЕР РАБОТ", 'purple');
        input_parameters << [$class: 'StringParameterDefinition', defaultValue: '', description: 'Номер плановых работ/номер таска<span style="color:red">*</span>', name: 'TICKET_NUMBER']

        //Инициализация основных параметров для вывода в userInput
        VARS.each{ k, v -> (k in ['RUN_JOB_FULLNAME','PASSWORD','AGENT_LABEL','SECRET_EXTRA_VARS'] ) ?: ( input_parameters << [$class: 'com.wangyin.ams.cms.abs.ParaReadOnly.WReadonlyStringParameterDefinition', defaultValue: v, description: '', name: k]) }

        //Запуск окна подтверждения
        timeout(time: 300, unit: 'SECONDS') { // timeout to abort this step
            userInput = input( id: 'userInput', message: 'Проверьте заполненные параметры и подтвердите действие', parameters: input_parameters);
        }

        //Проверка на ввод параметров
        if (userInput['TICKET_NUMBER'] == ''){
            common.abortPipeline('Не указан номер работ!!! Необходимо ввести номер работ!', false, true)
        }
    }

    // Запуск основного джоба
    stage ('Starting job: '+VARS['RUN_JOB_NAME']){
        
        //Подготовка основных переменных и инициализация классов параметров
        def triggered_params = [];
        triggered_params << [$class: 'WReadonlyStringParameterValue', name: 'TICKET_NUMBER', description: 'Номер плановых работ/номер таска', value: userInput['TICKET_NUMBER']]
        VARS.each{ k,v -> triggered_params = triggered_params + get_Deploy_Parameter(k,v)}

        echo(VARS['RUN_JOB_FULLNAME'])
        //Запуск триггера на результирующую джобу
        /// Исключаем не нужные параметры, во избежание ошибок в логах
        def resultParams = excludeParameters(triggered_params,VARS['RUN_JOB_FULLNAME'],common)
        //println resultParams
        //// Тригерим выполнения джоба
        def job_info = build( job: VARS['RUN_JOB_FULLNAME'], parameters: resultParams, propagate: false)

        // Вызов исключения, в случае если затриггерный билд не был успешно завершон
        if (job_info.getResult() != "SUCCESS"){
            common.abortPipeline("Build: ${job_info.getFullDisplayName()} is ${job_info.getResult()} \nRESULTS:\n${job_info.getCurrentResult()}", false, true)
        } 
        
    }
} catch (org.jenkinsci.plugins.workflow.steps.FlowInterruptedException e) {
    error 'Caught org.jenkinsci.plugins.workflow.steps.FlowInterruptedException'
} catch (exception) {
    
    // Формирование сообщения с исключением
    def error_msg = ''
    def stackTrace = exception.getStackTrace().join('\n')
    if (stackTrace.startsWith("jobrunner.Common.abortPipeline") ){
        error_msg = "EXCEPTION: \n${exception.toString()}"
    }else{
        error_msg = "EXCEPTION: \n${exception.toString()} \n\nMESSAGE: \n${exception.getMessage()} \n\nSTACK_TRACE: \n${exception.getStackTrace().join('\n')}"
    }
    def message = "PIPELINE FAILED! \n"+error_msg

    // Вызов метода по генерации ссылок на заведение баг и завершающего throw Exception
    common.abortPipeline(message, true, true )
}
